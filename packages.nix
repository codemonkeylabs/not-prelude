#
{dev, ...}: self: super: let

in {
  ghc = super.ghc;

  not-prelude = self.callCabal2nix "not-prelude" (
                 builtins.filterSource (path: type: type != "directory" ||
                                        baseNameOf path != ".git") ./. ) { };

  # overriden nixpkgs packages we need...

}
