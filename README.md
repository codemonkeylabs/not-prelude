# Not-Prelude - an opinionated Prelude replacement library
==========================================================

This library is an unscientific approach to reducing import boilerplate on those real world projects I work on. It is just so nice to write

```
runMaybeT $ tryThis <|> doThat
```

without having to import anything explicitly
